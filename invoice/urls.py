"""
invoice URL Configuration
"""
from django.urls import re_path

from invoice import views

app_name = 'invoice'
urlpatterns = [
  re_path(r'^order/$', views.OrderList.as_view()),
  re_path(r'^order/(?P<pk>\d+)/$', views.OrderView.as_view()),
  re_path(r'^customer/$', views.CustomerList.as_view()),
  re_path(r'^customer/(?P<pk>\d+)/$', views.CustomerView.as_view()),
  re_path(r'^products/$', views.ProductList.as_view()),
  re_path(r'^order-detail/$', views.OrderDetailList.as_view()),
  re_path(r'^users/$', views.UserList.as_view()),
]
