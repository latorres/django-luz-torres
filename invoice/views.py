from rest_framework import generics
from django.contrib.auth.models import User
from invoice.models import Order, Customer, Product, OrderDetail
from invoice.serializers import OrderSerializer, CustomerSerializer, ProductSerializer,OrderDetailSerializer, UserSerializer
from rest_framework_jwt.serializers import VerifyJSONWebTokenSerializer
from rest_framework import status
from rest_framework.response import Response



class OrderList(generics.ListCreateAPIView):
  """
  get:
  Return a list of all the existing orders.
  post:
  Create a new order instance.
  """

  queryset = Order.objects.all()
  serializer_class = OrderSerializer
  filter_fields = {'customer': ['exact'], 'date': ['range', 'exact']}


class UserList(generics.ListCreateAPIView):
  """
  get:
  Return a list of all the existing orders.
  post:
  Create a new order instance.
  """
 
  queryset = User.objects.all()
  serializer_class = UserSerializer
  
class OrderView(generics.RetrieveUpdateDestroyAPIView):

    serializer_class = OrderSerializer
    queryset = Order.objects.all()

# TODO completar el servicio para el cliente
class CustomerList(generics.ListCreateAPIView):
   """
   get:
   Return a list of all the existing customer.
   post:
   Create a new customer.
   """

   queryset = Customer.objects.all()
   serializer_class = CustomerSerializer



class CustomerView(generics.RetrieveUpdateDestroyAPIView):
   
    serializer_class = CustomerSerializer
    queryset = Customer.objects.all() 
    
    def get_user(request):
      http_autorization = request.META.get('HTTP_AUTHORIZATION', '')
      if http_autorization == "":
          user = request.user
      else:
          token = request.META.get('HTTP_AUTHORIZATION', '').split()[1]
          data = {'token': token}
          valid_data = VerifyJSONWebTokenSerializer().validate(data)
          user = valid_data['user']
      return user
    
    def delete(self, request,pk, *args, **kwargs):
       
       user = CustomerSerializer.get_user(self.request)
       customer = Customer.objects.get(pk=pk)
       customer.delete(deleted_by= user)
       return Response(status=status.HTTP_204_NO_CONTENT)

class ProductList(generics.ListAPIView):
  
   queryset = Product.objects.all()
   serializer_class = ProductSerializer
   

class OrderDetailList(generics.ListAPIView):
  
   queryset = OrderDetail.objects.all()
   serializer_class = OrderDetailSerializer

