from django.test import TestCase
import pycodestyle
from rest_framework import status
from rest_framework.test import APIClient
from django.contrib.auth.models import User
from invoice.models import Order, Customer, Product, OrderDetail

class DatoTestCase(TestCase):
  factory = APIClient()

  def test_login_get(self):
    response = self.factory.get('/api-token-auth/')
    self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

  def test_login_post_bad_request(self):
    response = self.factory.post('/api-token-auth/')
    self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    
  def setUp(self):
 
    user = User.objects.create(username="test2", email="test2@gmail.com", password="123456")
    user.set_password("123456")
    user.save()
    self.user = user

    self.data = {
      "correo": "test2@gmail.com",
      "contrasenia": "123456"
    }

  def test_login_post_ok(self):
   
    response = self.factory.post('/api-token-auth/', self.data)
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    self.token = response.data['token']


  def test_login_post_unprocessable_entity(self):
    
    data = {
      "correo": "test@grupomeiko.com",
      "contrasenia": "1sdf32fa1465aa879rwe"
    }
    response = self.factory.post('/api-token-auth/', data)
    self.assertEqual(response.status_code,
                     status.HTTP_422_UNPROCESSABLE_ENTITY)
    
  def test_order_get(self):
    response = self.factory.post('/api-token-auth/', self.data)
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    token = response.data['token']
#    print("************************************", token)
    self.factory.credentials(HTTP_AUTHORIZATION='Bearer {0}'.format(token))
    response = self.factory.get('/invoice/order/', headers={"Authorization": "Bearer " + token})
    self.assertEqual(response.status_code, status.HTTP_200_OK) 
    
  def test_customer_get(self):
    response = self.factory.post('/api-token-auth/', self.data)
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    token = response.data['token']
    self.factory.credentials(HTTP_AUTHORIZATION='Bearer {0}'.format(token))
    response = self.factory.get('/invoice/customer/', headers={"Authorization": "Bearer " + token})
    self.assertEqual(response.status_code, status.HTTP_200_OK) 
    
  def test_customer_get_401(self):
    
    self.factory.credentials(HTTP_AUTHORIZATION='Bearer {0}'.format("jdncunure"))
    response = self.factory.get('/invoice/customer/')
    self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED) 
    
  def test_order_post(self):
    response = self.factory.post('/api-token-auth/', self.data)
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    token = response.data['token']

    customer = Customer.objects.create(name="customer", email="customer@gmail.com")
    customer.save()
    data = {
      "delivery_address": "address",
      "date": "2019-06-01",
      "customer" : "1",
      "order_details": [{"product_id": 1,"quantity": 2}]    }

    self.factory.credentials(HTTP_AUTHORIZATION='Bearer {0}'.format(token))
    response = self.factory.post('/invoice/order/', data)
    self.assertEqual(response.status_code, status.HTTP_200_OK) 
    
  def test_customer_post(self):
    response = self.factory.post('/api-token-auth/', self.data)
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    token = response.data['token']

    data = {
      "name": "teste",
      "email": "t@gmail.com" }
    self.factory.credentials(HTTP_AUTHORIZATION='Bearer {0}'.format(token))
    response = self.factory.post('/invoice/customer/', data)
    self.assertEqual(response.status_code, status.HTTP_201_CREATED) 
  
  def test_customer_put(self):
    response = self.factory.post('/api-token-auth/', self.data)
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    token = response.data['token']

    customer = Customer.objects.create(name="customer", email="customer@gmail.com")
    customer.save()
    data = {
      "id": customer.id,
      "name": "teste",
      "email": "t@gmail.com" }
    self.factory.credentials(HTTP_AUTHORIZATION='Bearer {0}'.format(token))
    response = self.factory.put('/invoice/customer/'+ str(customer.id) + '/', data)
    self.assertEqual(response.status_code, status.HTTP_200_OK) 

    
  def test_customer_post_400(self):
    response = self.factory.post('/api-token-auth/', self.data)
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    token = response.data['token']

    customer = Customer.objects.create(name="customer", email="customer@gmail.com")
    customer.save()
    data = {
      "name": "teste",
      "emailx": "t@gmail.com" }
    self.factory.credentials(HTTP_AUTHORIZATION='Bearer {0}'.format(token))
    response = self.factory.post('/invoice/customer/', data)
    self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST) 
  
  def test_404(self):
    response = self.factory.post('/api-token-auth/', self.data)
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    token = response.data['token']
#    print("************************************", token)
    self.factory.credentials(HTTP_AUTHORIZATION='Bearer {0}'.format(token))
    response = self.factory.get('/invoice/order2/', headers={"Authorization": "Bearer " + token})
    self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


