from rest_framework import serializers
from invoice.models import AvailableProduct, Customer, Order
from invoice.models import OrderDetail, Product
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework_jwt.serializers import VerifyJSONWebTokenSerializer

class OrderSerializer(serializers.ModelSerializer):
  """
  order serializer.
  """
  total = serializers.SerializerMethodField(
    help_text='sum(quantity_i*product_price_i)')
  products = serializers.SerializerMethodField(
    help_text='comma separated list of products and quantities')
  order_details = serializers.JSONField(write_only=True,
                                        help_text='[{"product_id": 1,'
                                                  '"quantity": 2}]')
  class Meta:
    model = Order
    fields = ('id', 'delivery_address', 'date', 'customer', 
              'order_details', 'products', 'total')

  def get_user(request):
      http_autorization = request.META.get('HTTP_AUTHORIZATION', '')
      if http_autorization == "":
          user = request.user
      else:
          token = request.META.get('HTTP_AUTHORIZATION', '').split()[1]
          data = {'token': token}
          valid_data = VerifyJSONWebTokenSerializer().validate(data)
          user = valid_data['user']
      return user 
  # TODO completar el metodo para obtener el total
  def get_total(self, order):

    order_detail = OrderDetail.objects.filter(order= order)
    total = 0
    for order_p in order_detail:
        value = order_p.product.price * order_p.quantity
        total = total + value
    return total

  # TODO completar el metodo para obtener los productos
  def get_products(self, order):
    order_detail = OrderDetail.objects.filter(order= order)
    list_products = ''
    for order_p in order_detail:
        list_products = list_products  + str(order_p.quantity) + ' x ' + order_p.product.name + ", "
    return list_products[:-2]

  # TODO completar el metodo para crear una orden
  def create(self, validated_data):
    """
    Create and return a new `Order` instance, given the validated data.
    """
   
    user = OrderSerializer.get_user(self.context['request'])
    validated_data['created_by'] = user
    order_details_data = validated_data.pop('order_details')
    customer = validated_data['customer']
    order = Order.objects.create(**validated_data)
    
    for order_detail in order_details_data:
            count = 0 # contador de productos or orden (Permitir maximo 5 productos)
            product_order = Product.objects.get(id= order_detail['product_id'])
            ## validar si el producto esta permitido para el cliente
            product_available = AvailableProduct.objects.filter(product=product_order, customer= customer)
            if len(product_available) > 0 and count < 5:
                OrderDetail.objects.create(quantity=order_detail['quantity'], product=product_order, order=order)
                count = count + 1
            
    return order

  # TODO completar el metodo para actualizar una orden
  def update(self, instance, validated_data):
      user = OrderSerializer.get_user(self.context['request'])
      instance.delivery_address = validated_data.get('delivery_address', instance.delivery_address)
      instance.updated_by = user
      instance.save()
      return instance
   
class OrderDetailSerializer(serializers.ModelSerializer):
  """
  Order detail serializer.
  """
  product_name = serializers.SerializerMethodField(help_text='producto name')

  def get_product_name(self, order_detail):
    return order_detail.product.name
    
  def get_quantity(self, order_detail):
    return order_detail.quantity

  def get_created_date(self, order_detail):
    return order_detail.created_date

  def get_created_by(self, order_detail):
    return order_detail.get_created_by

  class Meta:
    model = OrderDetail
    fields = ('id', 'product_name', 'quantity')

class CustomerSerializer(serializers.ModelSerializer):
  """
  customer serializer.
  """
  
  def get_user(request):
      http_autorization = request.META.get('HTTP_AUTHORIZATION', '')
      if http_autorization == "":
          user = request.user
      else:
          token = request.META.get('HTTP_AUTHORIZATION', '').split()[1]
          data = {'token': token}
          valid_data = VerifyJSONWebTokenSerializer().validate(data)
          user = valid_data['user']
         
      return user 
  
  def get_text(self, customer):
    return customer.name

  def get_email(self, customer):
    return customer.email

  def create(self, validated_data):
      user = CustomerSerializer.get_user(self.context['request'])
      validated_data['created_by'] = user
      customer = Customer.objects.create(**validated_data)
      return customer
  
  def update(self, instance, validated_data):
      user = CustomerSerializer.get_user(self.context['request'])
      instance.updated_by = user
      instance.name = validated_data.get('name', instance.name)
      instance.email = validated_data.get('email', instance.email)
      instance.save()
      return instance
  
  class Meta:
    model = Customer    
    fields = ('id', 'name', 'email')
    
class UserSerializer(serializers.ModelSerializer):    
    class Meta:
        model = User
        fields = ('id', 'username')
    
class ProductSerializer(serializers.ModelSerializer):
  """
  product serializer.
  """
  name_product = serializers.SerializerMethodField(help_text='product name')
  def get_name_product(self, product):
    return product.name

  class Meta:
    model = Product
    fields = ('id', 'name_product')
